'use strict';

const GastoController = require('../controllers/gasto-controller')
const express = require('express')
const router = express.Router()



router
    .get('/conta/gastos', GastoController.getAll)
    .get('/conta/gasto', GastoController.addForm)
    .post('/conta/gastos', GastoController.save)
    .get('/conta/gasto/:gasto_id', GastoController.getOne)
    .put('/conta/gasto/:gasto_id', GastoController.save)
    .delete('/conta/gasto/:gasto_id', GastoController.delete)
    .use(GastoController.error404)


module.exports = router
