'use strict'

const GastoModel = require('../models/gasto-model')
const moment = require('moment')
const GastoController = () => {}

GastoController.getAll = (req, res, next) => {
    GastoModel.getAll((docs) => {
        let locals = {
            title: 'Lista de gastos',
            data: docs,
            moment: moment
        }
        res.render('index', locals)

    })
}

GastoController.getOne = (req, res, next) => {
    let gasto_id = req.params.gasto_id
    console.log(gasto_id)


    GastoModel.getOne(gasto_id, (docs) => {
        let locals = {
            title: 'Editar Gasto',
            data: docs,
            moment: moment
        }

        res.render('edit-gasto', locals)
    })

}

GastoController.save = (req, res, next) => {
    let gasto = {
        gasto_id: req.body.gasto_id,
        nombre: req.body.nombre,
        fecha: req.body.fecha,
        costo: req.body.costo,
        descripcion: req.body.descripcion,
        moment: moment
    };
    console.log(gasto)

    GastoModel.save(gasto, () => res.redirect('/conta/gastos'))

}

GastoController.delete = (req, res, next) => {
    let gasto_id = req.params.gasto_id
    console.log(gasto_id)


    GastoModel.delete(gasto_id, () => res.redirect('/conta/gastos'))

}

GastoController.addForm = (req, res, next) => res.render('add-gasto', {
    title: 'Agregar Gasto'
})

GastoController.error404 = (req, res, next) => {
    let error = new Error(),
        locals = {
            title: 'Error 404',
            description: 'Recurso no Encontrado.',
            error: error
        };

    error.status = 404;
    res.render('error', locals);
    next();

}



module.exports = GastoController
