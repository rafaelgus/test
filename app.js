'use stric'

const express = require('express')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const restFul = require('express-method-override')('_method')
const routes = require('./routes/gasto-router')
const faviconURL = `${__dirname}/public/img/node-favicon.png`
const publicDir = express.static(`${__dirname}/public`)
const viewDir = `${__dirname}/views`
const port = (process.env.PORT || 3000)
const app = express()


app
    .set('views', viewDir)
    .set('view engine', 'pug')
    .set('port', port)

    .use(favicon(faviconURL))
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({
        extended: false
    }))
    .use(restFul)
    .use(morgan('dev'))
    .use(publicDir)
    .use(routes)


module.exports = app
