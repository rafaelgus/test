'use strict'

const conn = require('./gasto-connection')
const GastoModel = () => {}

GastoModel.getAll = (cb) => {
    conn
        .find()
        .exec((err, docs) => {
            if (err) throw err
            cb(docs)
        })

}

GastoModel.getOne = (id, cb) => {
    conn
        .findOne({
            gasto_id: id
        })
        .exec((err, docs) => {
            if (err) throw err
            cb(docs)
        })

}

GastoModel.save = (data, cb) => {
    conn
        .count({
            gasto_id: data.gasto_id
        })
        .exec((err, count) => {
            if (err) throw err
            console.log(`Número de Docs: ${count}`)

            if (count == 0) {
                conn.create(data, (err) => {
                    if (err) throw err
                    cb()
                })
            } else if (count == 1) {
                conn.findOneAndUpdate({
                        gasto_id: data.gasto_id
                    }, {
                        nombre: data.nombre,
                        fecha: data.fecha,
                        costo: data.costo,
                        descripcion: data.descripcion
                    },
                    (err) => {
                        if (err) throw (err)
                        cb()
                    }
                )
            }
        })
}

GastoModel.delete = (id, cb) => {
    conn.remove({
        gasto_id: id
    }, (err, docs) => {
        if (err) throw err
        cb()
    })

}

GastoModel.close = () => coon.end()


module.exports = GastoModel
